System Requirements:

### Node.js => `>= v14.0.0`

In the project directory, you can run:

### `npm start`

to run the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in your browser.


### `npm run build`

to create build of the project

---------------------------------------------------------------------------------------------------------------

***  Additional Information ***

* Project is created using create-react-app
* Folder structure is designed keepinf the high level design of project in mind
* Redux Toolkit is used to showcase how store can be organized at high level
* Project uses axios for data fetching 


*** If API endpoint was a 'GET' method, then I would have used RTK Query. It is a more efficient way of fetching data, it also allows data caching and loading states for better state management.


