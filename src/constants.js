export const FILTER_CONSTANTS = {
    ROLE_OPTIONS: [
        { label: 'Frontend', value: 'frontend' },
        { label: 'Backend', value: 'backend' },
        { label: 'FullStack', value: 'fulstack' },
        { label: 'IOS', value: 'ios' },
        { label: 'Tech Lead', value: 'techlead' }
    ],
    EXPERIENCE_OPTIONS: [
        { label: '1', value: '1' },
        { label: '2', value: '2' },
        { label: '3', value: '3' },
        { label: '4', value: '4' },
        { label: '5+', value: '5' }
    ],
    REMOTE_OPTIONS: [
        { label: 'Remote', value: 'remote' },
        { label: 'Hybrid', value: 'hybrid' },
        { label: 'In-office', value: 'In-office' }
    ],
    BASE_SALARY_OPTIONS: [
        { label: '0k', value: '0' },
        { label: '10k', value: '10' },
        { label: '20k', value: '20' },
        { label: '30k', value: '30' },
        { label: '40k', value: '40' },
        { label: '50k +', value: '50' }
    ]
}