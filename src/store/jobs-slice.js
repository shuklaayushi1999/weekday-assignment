import { createSlice } from '@reduxjs/toolkit';

const jobsDataSlice = createSlice({
    name: 'jobsData',
    initialState: { jobs: null, filtered: null },
    reducers: {
        addJobs: (state, action) => {
            state.jobs = action.payload;
        },
        addFilteredJobs: (state, action) => {
            state.filtered = action.payload;
        }
    }
});

export const { addJobs, addFilteredJobs } = jobsDataSlice.actions;
export default jobsDataSlice;