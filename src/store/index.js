import { configureStore } from "@reduxjs/toolkit";
import jobsData from "./jobs-slice";

const store = configureStore({
    reducer: { 
        jobsData: jobsData.reducer
    }
});

export default store;