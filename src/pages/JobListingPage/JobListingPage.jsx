import * as React from 'react';
import axios from "axios";
import { Grid, Paper } from '@mui/material';
import CardSkeleton from '../../components/CardSkeleton/CardSkeleton';
import JobCard from '../../components/JobCard/JobCard';
import FilterToolbar from '../../components/FilterToolbar/FilterToolbar';
import { addFilteredJobs, addJobs } from '../../store/jobs-slice';
import { useDispatch, useSelector } from 'react-redux';

const JobListingPage = () => {

  const dispatch = useDispatch();
  const filteredData = useSelector((state) => state.jobsData.filtered);

  const [data, setData] = React.useState([{label:'', value: ''}]);
  const [loading, setLoading] = React.useState(true);
  const [hasMore, setHasMore] = React.useState(true);

  const [limit] = React.useState(10);
  const [offset, setOffset] = React.useState(0);

  const listRef = React.useRef(null);

  const fetchData = async () => {
    try {
      const response = await axios.post("https://api.weekday.technology/adhoc/getSampleJdJSON", { limit, offset: offset*limit });
      
      if(response.data.jdList.length < limit) setHasMore(false);
      setData((prev) => [...prev, ...response.data.jdList]);
      setLoading(false);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  React.useEffect(() => {
    fetchData();
  }, [offset]);

  React.useEffect(() => {
    dispatch(addJobs(data));
    dispatch(addFilteredJobs(data));
  }, [data]);

  const handleScroll = () => {
    const { scrollTop, clientHeight, scrollHeight } = listRef.current;
    if (scrollHeight - scrollTop === clientHeight && hasMore) {
        setOffset((prev) => prev + 1);
    }
  };

  return (
    <>
      <FilterToolbar/>
      <Paper elevation={0} sx={{ p: 4, pt: 0, maxHeight: '75vh', overflowY: 'auto' }} ref={listRef} onScroll={handleScroll}>
        <Grid container spacing={4}>
          {
            (loading ? Array.from(new Array(3)) : filteredData).map((item, index) => (
              item ?
                 (item?.companyName 
                  ? <Grid key={index} item xs={12} sm={6} md={4}>
                      <JobCard data={item} key={index} />
                    </Grid>
                    : null
                 )
                :
                (<Grid key={index} item xs={12} sm={6} md={4}>
                  <CardSkeleton key={index} />
                </Grid>)
            ))
          }
        </Grid>
      </Paper>
    </>
  );
};

export default JobListingPage;
