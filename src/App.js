import { Box } from '@mui/material';
import './App.css';
import JobListingPage from './pages/JobListingPage/JobListingPage';
import { Provider } from 'react-redux';
import store from './store';

function App() {
  return (
    <>
      <Provider store={store}>
        <Box sx={{
          height: '3rem',
          p: 2,
          color: '#fff',
          background: 'rgb(2,0,36)',
          background: 'linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(9,9,121,1) 35%, rgba(0,212,255,1) 100%)'
        }}>
          <h1 style={{ margin: 0, paddingLeft: '2.5rem' }}>Hi, User</h1>
        </Box>
        <JobListingPage />
      </Provider>
    </>
  );
}

export default App;
