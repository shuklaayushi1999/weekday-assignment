import * as React from 'react';
import { Grid } from '@mui/material';
import SelectDropdown from '../SelectDropdown/SelectDropdown';
import { FILTER_CONSTANTS } from '../../constants';
import { useSelector, useDispatch } from 'react-redux';
import { addFilteredJobs } from '../../store/jobs-slice';


const FilterToolbar = () => {

  const dispatch = useDispatch();
  const jobsData = useSelector((state) => state.jobsData.jobs);

  const sortFilters = [
    {
      title: "Role",
      placeholder: "Role",
      multiple: true,
      options: FILTER_CONSTANTS.ROLE_OPTIONS,
      value: { label: '', value: '' },
      handleChange: (value) => {
        const matchingJobs = [];
        console.log(value);
        if (value?.length) {
          jobsData.forEach(obj => {
              value.forEach(item => {
                if (obj?.jobRole?.toLowerCase() === item.value.toLowerCase()) {
                  matchingJobs.push(obj);
                }
              });
          });

          dispatch(addFilteredJobs(matchingJobs));
        }
        else
          dispatch(addFilteredJobs(jobsData));
      }
    },
    {
      title: "Experience",
      placeholder: "Experience",
      multiple: false,
      options: FILTER_CONSTANTS.EXPERIENCE_OPTIONS,
      value: { label: '', value: '' },
      handleChange: (value) => {
        if (value?.value) {
          const matchingJobs = jobsData.filter(obj => obj?.minExp && obj?.minExp >= value.value);
          console.log(matchingJobs);
          dispatch(addFilteredJobs(matchingJobs));
        }
        else
          dispatch(addFilteredJobs(jobsData));
      }
    },
    {
      title: "Remote/On-Site",
      placeholder: "Remote/On-Site",
      multiple: false,
      options: FILTER_CONSTANTS.REMOTE_OPTIONS,
      value: { label: '', value: '' },
      handleChange: (value) => { 
        if (value?.value) {
          const matchingJobs = jobsData.filter(obj => obj.location && 
            (obj?.location?.toLowerCase() === value?.value || (value?.value !== 'remote' && value.value !== 'hybrid'))
          );

          dispatch(addFilteredJobs(matchingJobs));
        }
        else
          dispatch(addFilteredJobs(jobsData));
      }
    },
    {
      title: "Base Salary",
      multiple: false,
      placeholder: "Minimum Base Pay Salary",
      options: FILTER_CONSTANTS.BASE_SALARY_OPTIONS,
      value: { label: '', value: '' },
      handleChange: (value) => {
        if (value?.value) {
          const matchingJobs = jobsData.filter(obj => obj?.minJdSalary && obj?.minJdSalary >= value.value);
          dispatch(addFilteredJobs(matchingJobs));
        }
        else
          dispatch(addFilteredJobs(jobsData));
      }
    }
  ];


  return (
    <>
      <h2 style={{ marginLeft: "3.5rem" }}> Search Jobs </h2>

      <Grid container px="3rem">
        {sortFilters.map((item, index) =>
          <div key={index} style={{ margin: '0.75rem ' }}>
            <SelectDropdown data={item} />
          </div>
        )}

      </Grid>
    </>
  );
}

export default FilterToolbar;
