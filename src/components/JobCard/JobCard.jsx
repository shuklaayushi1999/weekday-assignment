import * as React from 'react';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import { Button } from "@mui/material";

const JobCard = (props) => {

    const jobData = props.data;

    const [showFullDescription, setShowFullDescription] = React.useState(false);

    const toggleDescription = () => {
        setShowFullDescription(!showFullDescription);
    };

    return (
        <>
            <Card sx={{
                borderRadius: "20px",
                padding: "5px",
                boxShadow: "rgba(0, 0, 0, 0.25) 0px 1px 4px 0px",
                margin: "1rem"
            }}>
                <CardHeader
                    avatar={
                        <Avatar alt={jobData.companyName} src={jobData.logoUrl} />
                    }
                    title={jobData.companyName}
                    subheader={
                        <>
                            <Typography variant="body2" color="text.secondary" sx={{ textTransform: 'capitalize' }}>
                                {jobData.jobRole}
                            </Typography>
                            <Typography variant="caption" sx={{ textTransform: 'capitalize' }}>
                                {jobData.location}
                            </Typography>
                        </>
                    }
                />

                <CardContent sx={{ pt: 0 }}>

                    <Typography variant="body2" color="text.secondary" sx={{ fontSize: '14px', margin: '8px 0', fontWeight: 400 }}>
                        Estimated Salary: $ {jobData.minJdSalary ? jobData.minJdSalary + 'k' : ''} {jobData.minJdSalary && jobData.maxJdSalary ? '-' : ''} {jobData.maxJdSalary ? jobData.maxJdSalary + 'k' : ''}
                    </Typography>

                    <Typography variant="body2" color="text.secondary" sx={{ fontSize: '1rem', fontWeight: 500 }}>
                        About Company:
                    </Typography>
                    <Typography variant="caption" sx={{ fontSize: '14px', fontWeight: 600 }}>
                        About Us
                    </Typography>

                    <Typography variant="body2" color="text.secondary" sx={{ whiteSpace: 'pre-wrap' }}>
                        {showFullDescription || jobData.jobDetailsFromCompany.length <= 250
                            ? jobData.jobDetailsFromCompany
                            : `${jobData.jobDetailsFromCompany.slice(0, 250)}....`}
                        {jobData.jobDetailsFromCompany.length > 250 && (
                            <Button onClick={toggleDescription} sx={{textTransform: 'capitalize'}}>
                                {showFullDescription ? 'View Less' : 'View More'}
                            </Button>
                        )}
                    </Typography>

                    {jobData.minExp && jobData.maxExp &&
                        <Typography variant="body2" color="text.secondary" sx={{
                            fontSize: '13px',
                            fontWeight: 600,
                            letterSpacing: '1px',
                            marginBottom: '3px',
                            marginTop: '10px',
                            color: '#8b8b8b'
                        }}
                        >
                            Minimum Experience
                        </Typography>
                    }

                    <Typography variant="caption">
                        {jobData.minExp ? jobData.minExp : ''}
                        {jobData.minExp && jobData.maxExp && jobData.minExp !== jobData.maxExp ? '-' : ''}
                        {jobData.maxExp && jobData.minExp !== jobData.maxExp ? jobData.maxExp : ''} {jobData.minExp || jobData.maxExp ? 'Years' : ''}
                    </Typography>

                    <Button variant="contained" sx={{
                        width: "100%",
                        backgroundColor: "#55efc4",
                        color: "rgb(0, 0, 0)",
                        fontWeight: 600,
                        padding: "8px 18px",
                        marginTop: "0.8rem",
                        textTransform: "capitalize",
                        '&:hover': {
                            backgroundColor: "#3ccba3",
                        },
                    }} disableElevation>
                        Easy Apply
                    </Button>
                </CardContent>
            </Card>
        </>
    );
};

export default JobCard;
