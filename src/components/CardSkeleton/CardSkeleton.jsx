import * as React from 'react';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Skeleton from '@mui/material/Skeleton';

const CardSkeleton = () => {

    return (
        <>
            <Card sx={{
                borderRadius: "20px",
                padding: "5px",
                boxShadow: "rgba(0, 0, 0, 0.25) 0px 1px 4px 0px"
            }}>
                <CardHeader
                    avatar={
                        <Skeleton animation="wave" variant="circular" width={40} height={40} />
                    }
                    title={
                        <Skeleton
                            animation="wave"
                            height={15}
                            width="80%"
                            style={{ marginBottom: 3 }}
                        />
                    }
                    subheader={
                        <Skeleton animation="wave" height={15} width="40%" />
                    }
                />

                <CardContent>

                    <Skeleton sx={{ height: 190, marginBottom: 3 }} animation="wave" variant="rounded" />

                    <Skeleton animation="wave" height={15} style={{ marginBottom: 3 }} />
                    <Skeleton animation="wave" height={15} width="80%" />

                </CardContent>
            </Card>
        </>
    );
};

export default CardSkeleton;
