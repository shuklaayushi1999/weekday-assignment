import * as React from 'react';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';

const SelectDropdown = (props) => {

  const data = props.data;

  return (
    <Autocomplete
      multiple={data.multiple}
      limitTags={2}
      size="small"
      id={data.title}
      options={data.options}
      getOptionLabel={(option) => option.label}
      onChange={(event, value) => { data.handleChange(value) }}
      renderInput={(params) => (
        <TextField {...params} label={data.title} placeholder={data.placeholder} />
      )}
      sx={{ minWidth: '150px', maxWidth: '500px' }}
    />
  );
}

export default SelectDropdown;